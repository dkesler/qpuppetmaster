#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow) {
  ui->setupUi(this);
}

MainWindow::~MainWindow() { delete ui; }

void MainWindow::on_pbTestButton_clicked() {
  ui->lblLastEvent->setText("Push Me");
}

void MainWindow::on_pbOK_clicked() { ui->lblLastEvent->setText("OK"); }

void MainWindow::on_pbEdit_clicked() { ui->lblLastEvent->setText("Edit"); }

void MainWindow::on_pbDelete_clicked() { ui->lblLastEvent->setText("Delete"); }
