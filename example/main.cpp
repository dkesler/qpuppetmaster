#include "mainwindow.h"

#include <QApplication>
// PuppetMaster includes
#include <QProcessEnvironment>
#include <qpuppetmaster.hpp>

int main(int argc, char *argv[]) {
  QApplication a(argc, argv);
  MainWindow w;
  w.show();

  /*******************************************/
  /***** PuppetMaster integration: START *****/
  /*******************************************/
  auto env = QProcessEnvironment::systemEnvironment();
  //  if (env.contains("PUPPETMASTER")) {
  qDebug() << "PuppetMaster Started";
  quint16 port = env.value("PUPPETMASTER_PORT", "1234").toInt();
  qpuppetmaster::Server puppetMaster;
  puppetMaster.start(port);
  //  }
  /*******************************************/
  /***** PuppetMaster integration: END *******/
  /*******************************************/

  return a.exec();
}
