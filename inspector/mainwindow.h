#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <qpuppetmaster_client.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();

public slots:
  void screen_timeout();
  void mouse_pressed(const QPoint &);

private slots:
  void on_pbConnect_clicked();

private:
  Ui::MainWindow *ui;
  QPuppetMasterClient *client = nullptr;

  QTimer screen_update_timer;
  QPoint offset;
};

#endif // MAINWINDOW_H
