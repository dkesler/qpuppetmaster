#ifndef CONTENTVIEWER_H
#define CONTENTVIEWER_H

#include <QImage>
#include <QWidget>

class ContentViewer : public QWidget {
  Q_OBJECT
public:
  explicit ContentViewer(QWidget *parent = nullptr);

  void setBackgroundImage(const QImage &bg);
  void setSelection(const QRect &sr);
  void clearSelection();

signals:
  void pressedAt(const QPoint &);
public slots:

protected:
  void paintEvent(QPaintEvent *);
  void mousePressEvent(QMouseEvent *event);

private:
  QImage background;
  QRect selection;
};

#endif // CONTENTVIEWER_H
