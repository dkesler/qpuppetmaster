#include "contentviewer.h"
#include <QDebug>
#include <QMouseEvent>
#include <QPaintEvent>
#include <QPainter>

ContentViewer::ContentViewer(QWidget *parent) : QWidget(parent) {}

void ContentViewer::setBackgroundImage(const QImage &bg) {
  background = bg;
  this->repaint();
}

void ContentViewer::setSelection(const QRect &sr) {
  selection = sr;
  this->repaint();
}

void ContentViewer::clearSelection() { selection = QRect(); }

void ContentViewer::paintEvent(QPaintEvent *) {
  //
  if (background.isNull())
    return;

  QPainter painter(this);
  painter.drawImage(0, 0, background);

  if (!selection.isValid())
    return;

  QPen pen;
  pen.setBrush(Qt::red);
  pen.setWidth(2);
  painter.setPen(pen);
  painter.drawRect(selection);
}

void ContentViewer::mousePressEvent(QMouseEvent *event) {
  emit pressedAt(event->pos());
}
