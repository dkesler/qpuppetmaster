#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QClipboard>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow) {
  ui->setupUi(this);
  ui->stackedWidget->setCurrentIndex(0);

  connect(ui->contentView, &ContentViewer::pressedAt, this,
          &MainWindow::mouse_pressed);
}

MainWindow::~MainWindow() {
  delete ui;
  if (client)
    delete client;
}

void MainWindow::on_pbConnect_clicked() {
  client = new QPuppetMasterClient(ui->leServerAddress->text(),
                                   ui->leServerPort->text().toInt());
  ui->stackedWidget->setCurrentIndex(1);

  connect(&screen_update_timer, &QTimer::timeout, this,
          &MainWindow::screen_timeout);
  screen_update_timer.setInterval(1000);
  screen_update_timer.start();

  screen_timeout();
}

void MainWindow::mouse_pressed(const QPoint &pt) {

  auto m = pt + offset;

  auto resp = client->getElementAt(m.x(), m.y());
  if (!resp.valid) {
    qDebug() << "Failed to get element at position";
    return;
  }

  if (!resp.list.size()) {
    ui->statusBar->showMessage("No element found");
    return;
  }

  auto objName = resp.list[0].toString();

  qDebug() << objName;

  ui->statusBar->showMessage(objName);

  QClipboard *clipboard = QGuiApplication::clipboard();
  clipboard->setText(objName);

  //  resp = client->getElementProperties(objName, "geometry");

  //  if (!resp.valid) {
  //    qDebug() << "Failed to get element geometry";
  //    return;
  //  }

  //  auto rect = resp.map["geometry"].toRect();
  //  ui->contentView->setSelection(rect);
}

void MainWindow::screen_timeout() {
  auto resp = client->listWindows();

  if (!resp.valid) {
    qDebug() << "Invalid response";
    return;
  }

  if (resp.list.isEmpty()) {
    qDebug() << "No windows found";
    return;
  }

  auto windowObjName = resp.list[0].toString();

  resp = client->getElementProperties(windowObjName, "geometry");

  if (!resp.valid) {
    qDebug() << "Failed to get window properties";
    return;
  }

  auto rect = resp.map["geometry"].toRect();

  offset = rect.topLeft();

  this->resize(rect.width(), rect.height() + ui->statusBar->height());

  resp = client->getElementImage(windowObjName);

  if (!resp.valid) {
    qDebug() << "Failed to get window image";
    return;
  }

  auto ba = resp.list[0].toByteArray();

  auto image{QImage::fromData(ba, "PNG")};
  ui->contentView->setBackgroundImage(image);
}
