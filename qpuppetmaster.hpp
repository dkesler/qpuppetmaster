/**
 * MIT License
 *
 * Copyright (c) 2022 Daniel Kesler <kesler.daniel@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef QPUPPETMASTER_HPP
#define QPUPPETMASTER_HPP

#include <QAbstractItemView>
#include <QApplication>
#include <QBuffer>
#include <QComboBox>
#include <QCoreApplication>
#include <QDataStream>
#include <QDebug>
#include <QImage>
#include <QLineEdit>
#include <QMetaObject>
#include <QMetaProperty>
#include <QMouseEvent>
#include <QPixmap>
#include <QTcpServer>
#include <QTcpSocket>
#include <QTextEdit>
#include <QThread>
#include <QVariantMap>
#include <QWidget>
#include <QWindow>
#include <chrono>
#include <thread>

namespace qpuppetmaster {
enum class MouseAction { Press, Release, Click, DClick, Move };

enum class KeyAction { Press, Release, Click, Shortcut };

struct Command {
  bool valid = false;
  QString name;
  QVariantMap args;
};

/**
 * @brief PuppetMaster Client handler class
 */
class Client : public QObject {
  Q_OBJECT
public:
  Client(QTcpSocket *client) : m_client(client) {
    connect(client, &QAbstractSocket::disconnected, client,
            &QObject::deleteLater);
  }

  /**
   * @brief Sends a WELCOME message to the client.
   * @return true on success, false otherwise
   */
  bool sendWelcome() { return sendResponse("WELCOME"); }

  /**
   * @brief Sends an ERROR message to the client.
   * @return true on success, false otherwise
   */
  bool sendError() { return sendResponse("ERROR"); }

  /**
   * @brief Sends a response message to the client.
   * @param name Command name
   * @param arg_list Argument list
   * @param arg_map Argument map
   * @return true on success, false otherwise
   */
  bool sendResponse(const QString &name, const QVariantList &arg_list = {},
                    const QVariantMap &arg_map = {}) {
    QByteArray block;
    QDataStream ds_out(&block, QIODevice::WriteOnly);
    ds_out.setVersion(m_ds_version);

    ds_out << name << arg_list << arg_map;

    auto res = m_client->write(block);

    while (m_client->bytesToWrite()) {
      m_client->waitForBytesWritten();
    }

    return res;
  }

  /**
   * @brief Receive request from client.
   * @param msec Timeout in miliseconds
   * @return
   */
  Command recvRequest(int msec = 5000) {
    QDataStream ds_in(m_client);
    Command cmd;

    ds_in.setVersion(m_ds_version);
    do {
      if (!m_client->waitForReadyRead(msec)) {
        return cmd;
      }
      ds_in.startTransaction();
    } while (!ds_in.commitTransaction());

    ds_in >> cmd.name >> cmd.args;
    cmd.valid = true;

    return cmd;
  }

private:
  // TCP client socket
  QTcpSocket *m_client = nullptr;
  // DataStream version
  int m_ds_version = QDataStream::Qt_5_9;
};

/**
 * @brief PuppetMaster Server class
 */
class Server : public QObject {
  Q_OBJECT
public:
  Server() : m_server(this) {}

  /**
   * @brief Start server.
   * @param port Server port
   * @return true on success, false otherwise
   */
  bool start(quint16 port = 1234) {
    if (!m_server.listen(QHostAddress::Any, port)) {
      qDebug("Cannot start server");
      return false;
    }

    connect(&m_server, &QTcpServer::newConnection, this,
            &Server::newConnection);

    return true;
  }

public slots:
  /**
   * @brief newConnection handler
   */
  void newConnection() {
    QTcpSocket *clientConnection = m_server.nextPendingConnection();

    Client client{clientConnection};
    client.sendWelcome();

    auto cmd = client.recvRequest();

    if (!cmd.valid) {
      client.sendError();
      clientConnection->disconnectFromHost();
      return;
    }

    if (cmd.name == "LISTE") {
      QString class_name = "";
      if (cmd.args.contains("class")) {
        class_name = cmd.args["class"].toString();
      }
      QString prop_name = "";
      if (cmd.args.contains("prop")) {
        prop_name = cmd.args["prop"].toString();
      }

      auto list = listElements(class_name, prop_name);
      client.sendResponse(cmd.name, list);
    } else if (cmd.name == "LISTW") {
      QString class_name = "";
      if (cmd.args.contains("class")) {
        class_name = cmd.args["class"].toString();
      }

      auto list = listWindows(class_name);
      client.sendResponse(cmd.name, list);
    } else if (cmd.name == "GETP") {
      auto obj_name = cmd.args["name"].toString();

      QString prop_name = "";
      if (cmd.args.contains("prop")) {
        prop_name = cmd.args["prop"].toString();
      }

      auto props = getWidgetProperties(obj_name, prop_name);
      client.sendResponse(cmd.name, {}, props);
    } else if (cmd.name == "CLICK") {
      auto obj_name = cmd.args["name"].toString();
      auto res = mouseEventWidget(obj_name, MouseAction::Click);
      client.sendResponse(cmd.name, {res});
    } else if (cmd.name == "PRESS") {
      auto obj_name = cmd.args["name"].toString();
      auto res = mouseEventWidget(obj_name, MouseAction::Press);
      client.sendResponse(cmd.name, {res});
    } else if (cmd.name == "RELEASE") {
      auto obj_name = cmd.args["name"].toString();
      auto res = mouseEventWidget(obj_name, MouseAction::Release);
      client.sendResponse(cmd.name, {res});
    } else if (cmd.name == "INPUT") {
      auto obj_name = cmd.args["name"].toString();
      auto text = cmd.args["text"].toString();
      auto res = sendKeysToWidget(obj_name, KeyAction::Click, text);
      client.sendResponse(cmd.name, {res});
    } else if (cmd.name == "CLEAR") {
      auto obj_name = cmd.args["name"].toString();
      auto res = clearWidgetInput(obj_name);
      client.sendResponse(cmd.name, {res});
    } else if (cmd.name == "SELECT") {
      auto obj_name = cmd.args["name"].toString();
      int row = cmd.args["row"].toInt();
      int col = cmd.args["col"].toInt();
      auto res = selectListItem(obj_name, row, col);
      client.sendResponse(cmd.name, {res});
    } else if (cmd.name == "ITEMS") {
      auto obj_name = cmd.args["name"].toString();
      auto items = getElementItems(obj_name);
      client.sendResponse(cmd.name, items);
    } else if (cmd.name == "WAIT") {
      auto obj_name = cmd.args["name"].toString();
      int timeout = cmd.args["timeout"].toInt();
      auto res = waitForElement(obj_name, timeout);
      client.sendResponse(cmd.name, {res});
    } else if (cmd.name == "IMAGE") {
      auto obj_name = cmd.args["name"].toString();
      auto image = getWidgetImage(obj_name);
      client.sendResponse(cmd.name, {image});
    } else if (cmd.name == "ELAT") {
      int x = cmd.args["x"].toInt();
      int y = cmd.args["y"].toInt();
      auto res = getWidgetAt(x, y);
      client.sendResponse(cmd.name, {res});
    } else {
      client.sendError();
    }

    clientConnection->disconnectFromHost();
  }

private:
  // Server instance
  QTcpServer m_server;

  /**
   * @brief Checks whether the given metaobject is or inharits a specific class
   * @param[in] mo MetaObject instance
   * @param[in] class_name Class name
   * @return true if yes, false otherwise
   */
  bool hasSuperClass(const QMetaObject *mo, const QString &class_name) {
    if (mo->className() == class_name) {
      return true;
    }

    auto parent = mo->superClass();
    if (parent) {
      return hasSuperClass(parent, class_name);
    }

    return false;
  }

  /**
   * @brief Checks whether the given metaobject is or inharits a specific class
   * @param[in] widget Widget instance
   * @param[in] class_name Class name
   * @return true if yes, false otherwise
   */
  bool inheritsClass(const QWidget *widget, const QString &class_name) {
    const QMetaObject *mo = widget->metaObject();
    return hasSuperClass(mo, class_name);
  }

  /**
   * @brief Find widget by object name
   * @param[in] obj_name Object name
   * @return Widget instance or nullptr
   */
  QWidget *findWidgetByName(const QString &obj_name) {
    for (auto &w : QApplication::allWidgets()) {
      if (w->objectName() == obj_name) {
        return w;
      }
    }
    return nullptr;
  }

  /**
   * @brief Get a list of available windows.
   * @param[in] obj_path Filter by class if not empty
   * @return List of window object names
   */
  QWidget *findWidgetByPath(const QString &obj_path) {
    for (auto &w : QApplication::topLevelWidgets()) {
      if (w->objectName().isEmpty())
        continue;

      if (!w->isVisible())
        continue;

      QString parent_path = w->objectName();

      if (parent_path == obj_path)
        return w;

      QWidget *w2 = findChildWidget(w, obj_path, parent_path);

      if (w2)
        return w2;
    }
    return nullptr;
  }

  /**
   * @brief findChildWidget
   * @param[in] parent
   * @param[in] obj_path
   * @param[in] parent_path
   * @return
   */
  QWidget *findChildWidget(const QObject *parent, const QString &obj_path,
                           const QString &parent_path) {
    for (auto &w : parent->children()) {
      if (w->objectName().isEmpty())
        continue;

      QString cur_path = parent_path + "." + w->objectName();

      if (cur_path == obj_path) {
        return dynamic_cast<QWidget *>(w);
      }

      QWidget *w2 = findChildWidget(w, obj_path, cur_path);
      if (w2)
        return w2;
    }

    return nullptr;
  }

  /**
   * @brief Get the full object path all the way to the top parent.
   * @param[in] obj Starting object
   * @return Full object path
   */
  QString fullObjectPath(const QObject *obj) {
    QString path = obj->objectName();
    auto parent = obj->parent();
    if (parent) {
      path = fullObjectPath(parent) + "." + path;
    }
    return path;
  }

  /**
   * @brief Send a key event to a widget.
   * @param[in] action Key action (Press, Release, Click...)
   * @param[in] widget Widget instance
   * @param[in] key Key value
   */
  void keyEvent(KeyAction action, QWidget *widget, const QString &key) {
    if (action == KeyAction::Click) {
      keyEvent(KeyAction::Press, widget, key);
      keyEvent(KeyAction::Release, widget, key);
      return;
    }

    QEvent::Type type = QEvent::KeyPress;
    if (action == KeyAction::Press) {
      type = QEvent::KeyPress;
    } else if (action == KeyAction::Release) {
      type = QEvent::KeyRelease;
    }
    QKeyEvent ke(type, 0, 0, key);

    if (!qApp->notify(widget, &ke)) {
      qDebug() << "key event not accepted by receiving widget";
    }
    QApplication::processEvents();
  }

  /**
   * @brief Send mouse event to a widget.
   * @param[in] action Mouse action (Press, Release, Click...)
   * @param[inout] widget Widget instance
   * @param[in] button Mouse button
   * @param[in] stateKey Keyboard modifier state
   * @param[in] pos Mouse position if needed
   */
  void mouseEvent(MouseAction action, QWidget *widget, Qt::MouseButton button,
                  Qt::KeyboardModifiers stateKey = 0, QPoint pos = QPoint()) {
    if (pos.isNull()) {
      auto rect = widget->rect();
      pos = QPoint(2, rect.height() / 2);
    }

    stateKey &= static_cast<unsigned int>(Qt::KeyboardModifierMask);

    if (action == MouseAction::Click) {
      mouseEvent(MouseAction::Press, widget, button, stateKey, pos);
      //            QThread::msleep(20);
      mouseEvent(MouseAction::Release, widget, button, stateKey, pos);
      return;
    }

    QMouseEvent me =
        QMouseEvent(QEvent::User, QPoint(), Qt::LeftButton, button, stateKey);

    switch (action) {
      //    case MouseAction::MouseDClick:
      //      QApplication::sendEvent(widget, &press);
      //      QApplication::sendEvent(widget, &release);
      //      Q_FALLTHROUGH();

    case MouseAction::Press:
      me = QMouseEvent(QEvent::MouseButtonPress, pos, widget->mapToGlobal(pos),
                       button, button, stateKey);
      break;

    case MouseAction::Release:
      me =
          QMouseEvent(QEvent::MouseButtonRelease, pos, widget->mapToGlobal(pos),
                      button, Qt::MouseButton(), stateKey);
      break;
    case MouseAction::Move:
      QCursor::setPos(widget->mapFromGlobal(pos));
      break;
    default:;
    }

    QApplication::processEvents();
    if (!qApp->notify(widget, &me)) {
      qDebug() << "mouse event not accepted by receiving widget";
    }
    QApplication::processEvents();
  }

  /**
   * @brief Sends a sequence of keystrokes to a widget.
   * @param name Widget object name.
   * @param action Key action.
   * @param text Key sequence.
   * @return true on success, false otherwise
   */
  bool sendKeysToWidget(const QString &name, KeyAction action,
                        const QString &text) {
    auto w = findWidgetByPath(name);
    if (!w)
      return false;

    for (auto &ch : text) {
      keyEvent(action, w, ch);
    }

    return true;
  }

  bool clearWidgetInput(const QString &name) {
    auto w = findWidgetByPath(name);
    if (!w)
      return false;

    if (inheritsClass(w, "QLineEdit")) {
      auto le = dynamic_cast<QLineEdit *>(w);
      if (le) {
        le->clear();
      }
    } else if (inheritsClass(w, "QTextEdit")) {
      auto te = dynamic_cast<QTextEdit *>(w);
      if (te) {
        te->clear();
      }
    } else {
      return false;
    }

    return true;
  }

  /**
   * @brief Sends a mouse event to a widget.
   * @param name Widget object name.
   * @return true on success, false otherwise
   */
  bool mouseEventWidget(const QString &name, MouseAction action,
                        Qt::MouseButton button = Qt::LeftButton) {
    auto w = findWidgetByPath(name);
    if (!w)
      return false;

    mouseEvent(action, w, button);
    return true;
  }

  /**
   * @brief Get a list of available elements.
   * @param class_name Filter by class if not empty
   * @return List of element object names
   */
  QVariantList listElements(const QString &class_name,
                            const QString &prop_name) {
    QVariantList elements;
    for (auto &w : QApplication::topLevelWidgets()) {
      const QMetaObject *mo = w->metaObject();

      if (w->objectName().isEmpty())
        continue;

      if (!w->isVisible())
        continue;

      QString element_path = w->objectName();

      if (class_name.isEmpty()) {
        elements << element_path;
      } else {
        if (hasSuperClass(mo, class_name)) {
          elements << element_path;
        }
      }

      for (QObject *c : w->children()) {
        const QMetaObject *mo2 = c->metaObject();

        if (c->objectName().isEmpty())
          continue;

        QString path = w->objectName() + "." + c->objectName();

        element_path = path;
        element_path += ":" + QString(mo2->className());

        if (!prop_name.isEmpty()) {
          auto value = c->property(prop_name.toStdString().c_str());
          if (value.isValid()) {
            element_path += " [" + value.toString() + "]";
          }
        }

        if (!prop_name.isEmpty()) {
        }

        if (class_name.isEmpty()) {
          elements << element_path;
        } else {
          if (hasSuperClass(mo2, class_name)) {
            elements << element_path;
          }
        }

        listChildWidgets(c, path, class_name, prop_name, elements);
      }
    }
    return elements;
  }

  /**
   * @brief List child widgets and apply filters to the list.
   * @param[in] obj Parent object
   * @param[in] parent_path Parent Path
   * @param[in] class_name Class to filter by
   * @param[in] prop_name Include property value in output list
   * @param[out] list Output list
   */
  void listChildWidgets(QObject *obj, const QString &parent_path,
                        const QString &class_name, const QString &prop_name,
                        QVariantList &list) {
    for (auto &c : obj->children()) {
      const QMetaObject *mo = c->metaObject();
      QString path = parent_path + "." + c->objectName();

      if (c->objectName().isEmpty())
        continue;

      QString element_path = path + ":" + mo->className();

      if (!prop_name.isEmpty()) {
        auto value = c->property(prop_name.toStdString().c_str());
        if (value.isValid()) {
          element_path += " [" + value.toString() + "]";
        }
      }

      if (class_name.isEmpty()) {
        list << element_path;
      } else {
        if (hasSuperClass(mo, class_name)) {
          list << element_path;
        }
      }

      listChildWidgets(c, path, class_name, prop_name, list);
    }
  }

  /**
   * @brief Get a list of available windows.
   * @param[in] class_name Filter by class if not empty
   * @return List of window object names
   */
  QVariantList listWindows(const QString &class_name) {
    QVariantList elements;
    for (auto &w : QApplication::topLevelWidgets()) {
      if (w->objectName().isEmpty())
        continue;

      if (!w->isVisible())
        continue;

      if (!class_name.isEmpty()) {
        const QMetaObject *mo = w->metaObject();
        if (mo->className() != class_name)
          continue;
      }

      elements << w->objectName();
    }
    return elements;
  }

  /**
   * @brief Get widget properties.
   * @param obj_name Widget object name
   * @param prop_name Property name or empty to get all properties
   * @return Map of property name/value pairs
   */
  QVariantMap getWidgetProperties(const QString &obj_name,
                                  const QString &prop_name) {
    QVariantMap props;
    auto w = findWidgetByPath(obj_name);
    if (!w)
      return props;

    const QMetaObject *mo = w->metaObject();
    QStringList supported = {"QString", "bool", "int", "QRect", "QPoint"};

    for (int i = 0; i < mo->propertyCount(); ++i) {
      auto propName = QString::fromLatin1(mo->property(i).name());
      auto value = w->property(mo->property(i).name());

      //      qDebug() << propName << " = " << value;

      // Only add properties with supported types
      if (!supported.contains(value.typeName()))
        continue;

      if (prop_name.isEmpty()) {
        props[propName] = value;
      } else {
        // TODO: allow glob filtering
        // Only return requested property
        if (propName == prop_name) {
          props[propName] = value;
          return props;
        }
      }
    }

    return props;
  }

  /**
   * @brief Get items of an list element.
   * @param obj_name Widget object name
   * @return true on success, false otherwise
   */
  QVariantList getElementItems(const QString &obj_name) {
    QVariantList items;
    auto w = findWidgetByPath(obj_name);
    if (!w)
      return items;

    if (inheritsClass(w, "QAbstractItemView")) {
      auto lv = dynamic_cast<QAbstractItemView *>(w);
      auto count = lv->model()->rowCount();
      for (int row = 0; row < count; row++) {
        //
        auto mi = lv->model()->index(row, 0);
        auto text = lv->model()->data(mi, Qt::DisplayRole).toString();

        items.append(text);
      }
    } else if (inheritsClass(w, "QComboBox")) {
      auto cb = dynamic_cast<QComboBox *>(w);
      for (int row = 0; row < cb->count(); row++) {
        items.append(cb->itemText(row));
      }
    }

    return items;
  }

  /**
   * @brief Select an item of an list element.
   * @param obj_name Widget object name
   * @param row Item row
   * @param col Item column
   * @return true on success, false otherwise
   */
  bool selectListItem(const QString &obj_name, int row, int col) {
    auto w = findWidgetByPath(obj_name);
    if (!w)
      return false;

    if (inheritsClass(w, "QAbstractItemView")) {
      auto lv = dynamic_cast<QAbstractItemView *>(w);
      auto mi = lv->model()->index(row, col);
      lv->setCurrentIndex(mi);
    } else if (inheritsClass(w, "QComboBox")) {
      auto cb = dynamic_cast<QComboBox *>(w);
      cb->setCurrentIndex(row);
    } else {
      // TODO: add more classes
    }

    return false;
  }

  /**
   * @brief waitForElement
   * @param obj_name
   * @param timeout
   * @return
   */
  bool waitForElement(const QString &obj_name, int timeout) {
    while (timeout) {
      auto w = findWidgetByPath(obj_name);
      if (w) {
        return true;
      }

      std::this_thread::sleep_for(std::chrono::milliseconds(100));

      timeout -= 100;
      if (timeout < 0)
        timeout = 0;
    }

    return false;
  }

  /**
   * @brief getWidgetImage
   * @param obj_name
   * @return
   */
  QByteArray getWidgetImage(const QString &obj_name) {
    auto w = findWidgetByPath(obj_name);

    if (!w)
      return QByteArray();

    auto pixmap = w->grab();
    auto image = pixmap.toImage();

    QByteArray ba;
    QBuffer buffer(&ba);
    buffer.open(QIODevice::WriteOnly);
    image.save(&buffer, "PNG");

    return ba;
  }

  QVariantList getWidgetAt(int x, int y) {
    QVariantList items;

    auto w = QApplication::widgetAt(x, y);

    if (w) {
      items.append(fullObjectPath(w));
    }

    return items;
  }
};

} // namespace qpuppetmaster

#endif // QPUPPETMASTER_HPP
