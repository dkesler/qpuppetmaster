/**
 * MIT License
 *
 * Copyright (c) 2022 Daniel Kesler <kesler.daniel@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "qpuppetmaster_client.h"
#include <QDataStream>
#include <QFile>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <iostream>

QPuppetMasterClient::QPuppetMasterClient(const QString &host, qint16 port,
                                         OutputFormat format)
    : m_socket(this), m_host(host), m_port(port), m_format(format) {}

QPuppeteerResponse QPuppetMasterClient::sendRequest(const QString &command,
                                                    const QVariantMap &args,
                                                    int timeout) {
  if (!connectToServer()) {
    return QPuppeteerResponse{};
  }

  QByteArray block;
  QDataStream ds(&block, QIODevice::WriteOnly);
  ds.setVersion(m_ds_version);

  ds << command << args;

  m_socket.write(block);

  return recvResponse(timeout + 600);
}

qint64 QPuppetMasterClient::sendRequestAndShowResponse(const QString &command,
                                                       const QVariantMap &args,
                                                       int timeout) {

  auto resp = sendRequest(command, args, timeout);

  return showResponse(resp);
}

QPuppeteerResponse QPuppetMasterClient::recvResponse(int msec) {
  QPuppeteerResponse resp;
  if (!m_socket.waitForConnected()) {
    return resp;
  }

  QDataStream ds(&m_socket);
  ds.setVersion(m_ds_version);

  do {
    if (!m_socket.waitForReadyRead(msec)) {
      qDebug() << "Did not receive ANY response\n";
      return resp;
    }

    ds.startTransaction();
    ds >> resp.name >> resp.list >> resp.map;
  } while (!ds.commitTransaction());

  resp.valid = (resp.name != "ERROR");

  return resp;
}

bool QPuppetMasterClient::connectToServer() {
  m_socket.abort();
  m_socket.connectToHost(m_host, m_port);

  auto welcome = recvResponse();
  if (!welcome.valid || welcome.name != "WELCOME") {
    qDebug() << "Did not receive WELCOME message";
    return false;
  }

  return true;
}

int QPuppetMasterClient::showResponse(const QPuppeteerResponse &resp) {
  if (!resp.valid) {
    QJsonObject out;
    out["error"] = true;
    out["list"] = QJsonArray();
    out["map"] = QJsonObject();
    return -2;
  }

  if (m_format == OutputFormat::JSON) {
    QJsonArray jslist;
    for (auto &v : resp.list) {
      jslist.append(QJsonValue::fromVariant(v));
    }

    QJsonObject jsobj;
    for (auto &k : resp.map.keys()) {
      jsobj[k] = QJsonValue::fromVariant(resp.map[k]);
    }

    QJsonObject out;
    out["error"] = false;
    out["list"] = jslist;
    out["map"] = jsobj;

    QJsonDocument json(out);
    std::cout << json.toJson(QJsonDocument::Indented).toStdString();
  } else {
    qDebug("Unknown output format");
    return -3;
  }

  return 0;
}

QPuppeteerResponse
QPuppetMasterClient::listElements(const QString &filterByClass,
                                  const QString &prop_name) {
  return sendRequest("LISTE", {{"class", filterByClass}, {"prop", prop_name}});
}

QPuppeteerResponse
QPuppetMasterClient::listWindows(const QString &filterByClass) {
  return sendRequest("LISTW", {{"class", filterByClass}});
}

QPuppeteerResponse
QPuppetMasterClient::getElementProperties(const QString &name,
                                          const QString &filterByName) {
  return sendRequest("GETP", {{"name", name}, {"prop", filterByName}});
}

QPuppeteerResponse QPuppetMasterClient::clickOnElement(const QString &name) {
  return sendRequest("CLICK", {{"name", name}});
}

QPuppeteerResponse QPuppetMasterClient::pressElement(const QString &name) {
  return sendRequest("PRESS", {{"name", name}});
}

QPuppeteerResponse QPuppetMasterClient::releaseElement(const QString &name) {
  return sendRequest("RELEASE", {{"name", name}});
}

QPuppeteerResponse QPuppetMasterClient::sendTextToElement(const QString &name,
                                                          const QString &text) {
  return sendRequest("INPUT", {{"name", name}, {"text", text}});
}

QPuppeteerResponse QPuppetMasterClient::clearElement(const QString &name) {
  return sendRequest("CLEAR", {{"name", name}});
}

QPuppeteerResponse QPuppetMasterClient::selectListItem(const QString &name,
                                                       int row, int col) {
  return sendRequest("SELECT", {{"name", name}, {"row", row}, {"col", col}});
}

QPuppeteerResponse QPuppetMasterClient::getElementItems(const QString &name) {
  return sendRequest("ITEMS", {{"name", name}});
}

QPuppeteerResponse QPuppetMasterClient::waitForElement(const QString &name,
                                                       int timeout) {
  return sendRequest("WAIT", {{"name", name}, {"timeout", timeout}}, timeout);
}

QPuppeteerResponse QPuppetMasterClient::getElementImage(const QString &name) {

  return sendRequest("IMAGE", {{"name", name}});

  //  if (!resp.valid) {
  //    return -1;
  //  }

  //  QByteArray ba = resp.list[0].toByteArray();
  //  QFile file(filename);
  //  if (!file.open(QFile::WriteOnly))
  //    return -1;

  //  file.write(ba);
  //  file.close();

  //  return 1;
}

QPuppeteerResponse QPuppetMasterClient::getElementAt(int x, int y) {
  return sendRequest("ELAT", {{"x", x}, {"y", y}});
}
