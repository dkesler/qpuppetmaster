/**
 * MIT License
 *
 * Copyright (c) 2022 Daniel Kesler <kesler.daniel@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#ifndef QPUPPETMASTERCLIENT_H
#define QPUPPETMASTERCLIENT_H

#include <QCoreApplication>
#include <QDataStream>
#include <QDebug>
#include <QHostAddress>
#include <QString>
#include <QTcpSocket>

enum class OutputFormat { JSON };

struct QPuppeteerResponse {
  bool valid = false;
  QString name;
  QVariantList list;
  QVariantMap map;
};

class QPuppetMasterClient : public QObject {
  Q_OBJECT
public:
  /**
   * @brief QPuppeteerClient
   * @param host
   * @param port
   * @param format
   */
  QPuppetMasterClient(const QString &host, qint16 port,
                      OutputFormat format = OutputFormat::JSON);

  /**
   * @brief listElements
   * @param filterByClass
   * @return
   */
  QPuppeteerResponse listElements(const QString &filterByClass = "",
                                  const QString &prop_name = "");

  /**
   * @brief listWindows
   * @param filterByClass
   * @return
   */
  QPuppeteerResponse listWindows(const QString &filterByClass = "");

  /**
   * @brief getElementProperties
   * @param name
   * @param filterByName
   * @return
   */
  QPuppeteerResponse getElementProperties(const QString &name,
                                          const QString &filterByName = "");

  /**
   * @brief clickOnElement
   * @param name
   * @return
   */
  QPuppeteerResponse clickOnElement(const QString &name);

  /**
   * @brief pressElement
   * @param name
   * @return
   */
  QPuppeteerResponse pressElement(const QString &name);

  /**
   * @brief releaseElement
   * @param name
   * @return
   */
  QPuppeteerResponse releaseElement(const QString &name);

  /**
   * @brief sendTextToElement
   * @param name
   * @param text
   * @return
   */
  QPuppeteerResponse sendTextToElement(const QString &name,
                                       const QString &text);

  /**
   * @brief clearElement
   * @param name
   * @return
   */
  QPuppeteerResponse clearElement(const QString &name);

  /**
   * @brief selectListItem
   * @param name
   * @param row
   * @param col
   * @return
   */
  QPuppeteerResponse selectListItem(const QString &name, int row, int col);

  /**
   * @brief getElementItems
   * @param name
   * @return
   */
  QPuppeteerResponse getElementItems(const QString &name);

  /**
   * @brief waitForElement
   * @param name
   * @param timeout
   * @return
   */
  QPuppeteerResponse waitForElement(const QString &name, int timeout);

  /**
   * @brief getElementImage
   * @param name
   * @return
   */
  QPuppeteerResponse getElementImage(const QString &name);

  /**
   * @brief getElementFromPosition
   * @param x
   * @param y
   * @return
   */
  QPuppeteerResponse getElementAt(int x, int y);

private:
  // Client socket
  QTcpSocket m_socket;
  // Server address
  QHostAddress m_host;
  // Server port
  qint16 m_port;
  // Output format
  OutputFormat m_format = OutputFormat::JSON;
  // DataStream version
  int m_ds_version = QDataStream::Qt_5_9;

  /**
   * @brief connectToServer
   * @return
   */
  bool connectToServer();

  QPuppeteerResponse sendRequest(const QString &command,
                                 const QVariantMap &args, int timeout = 1000);

  qint64 sendRequestAndShowResponse(const QString &command,
                                    const QVariantMap &args,
                                    int timeout = 1000);

  /**
   * @brief recvResponse
   * @param msec
   * @return
   */
  QPuppeteerResponse recvResponse(int msec = 5000);

  /**
   * @brief showResponse
   * @param resp
   */
  int showResponse(const QPuppeteerResponse &resp);
};

#endif // QPUPPETMASTERCLIENT_H
