PuppetMaster is a simple QT introspect server/client addon that allows to simulate maual interaction with QT applications.
The intended usage is to automate UI testing of local or remote applications.