#include <cstdint>
#include <map>
#include <string>

#include <CLI11.hpp>
#include <QFile>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>

#include <qpuppetmaster_client.h>

int showResponse(const QPuppeteerResponse &resp, OutputFormat format) {
  if (!resp.valid) {
    QJsonObject out;
    out["error"] = true;
    out["list"] = QJsonArray();
    out["map"] = QJsonObject();
    return -2;
  }

  if (format == OutputFormat::JSON) {
    QJsonArray jslist;
    for (auto &v : resp.list) {
      jslist.append(QJsonValue::fromVariant(v));
    }

    QJsonObject jsobj;
    for (auto &k : resp.map.keys()) {
      jsobj[k] = QJsonValue::fromVariant(resp.map[k]);
    }

    QJsonObject out;
    out["error"] = false;
    out["list"] = jslist;
    out["map"] = jsobj;

    QJsonDocument json(out);
    std::cout << json.toJson(QJsonDocument::Indented).toStdString();
  } else {
    qDebug("Unknown output format");
    return -3;
  }

  return 0;
}

int main(int argc, char *argv[]) {

  CLI::App appargs{"PuppetMaster Client"};

  std::string host = "127.0.0.1";
  uint16_t port = 1234;
  OutputFormat format{OutputFormat::JSON};
  std::map<std::string, OutputFormat> map{{"json", OutputFormat::JSON}};

  std::string second_arg = "";
  std::string obj_name = "";
  int iarg1 = 0, iarg2 = 0;

  appargs.add_option("-H,--host", host, "PupperMaster address [" + host + "]");
  appargs.add_option("-P,--port", port,
                     "PupperMaster port [" + std::to_string(port) + "]");

  appargs.add_option("-f,--format", format, "Output format")
      ->transform(CLI::CheckedTransformer(map, CLI::ignore_case));

  auto *liste_cmd = appargs.add_subcommand("liste", "List visible elements");
  liste_cmd->add_option("-c", second_arg, "Filter by class");
  liste_cmd->add_option("-p", obj_name, "Property value to append");

  auto *listw_cmd = appargs.add_subcommand("listw", "List visible windows");
  listw_cmd->add_option("-c", second_arg, "Filter by class");

  auto *getp_cmd = appargs.add_subcommand("prop", "Get element properties");
  getp_cmd->add_option("name", obj_name, "Element name")->required();
  getp_cmd->add_option("prop", second_arg, "Property name");

  auto *click_cmd = appargs.add_subcommand("click", "Click on element");
  click_cmd->add_option("name", obj_name, "Element name")->required();

  auto *press_cmd = appargs.add_subcommand("press", "Press element");
  press_cmd->add_option("name", obj_name, "Element name")->required();

  auto *release_cmd = appargs.add_subcommand("release", "Press element");
  release_cmd->add_option("name", obj_name, "Element name")->required();

  auto *input_cmd =
      appargs.add_subcommand("input", "Send key sequence to element");
  input_cmd->add_option("name", obj_name, "Element name")->required();
  input_cmd->add_option("text", second_arg, "Input text")->required();

  auto *clear_cmd = appargs.add_subcommand("clear", "Clear element input text");
  clear_cmd->add_option("name", obj_name, "Element name")->required();

  auto *select_cmd = appargs.add_subcommand("select", "Select list item");
  select_cmd->add_option("name", obj_name, "List element name")->required();
  select_cmd->add_option("row", iarg1, "Item row index")->required();
  select_cmd->add_option("col", iarg2, "Item column index");

  auto *items_cmd = appargs.add_subcommand("items", "Get element items");
  items_cmd->add_option("name", obj_name, "Element name")->required();

  auto *wait_cmd =
      appargs.add_subcommand("wait", "Wait for an element to be available");
  wait_cmd->add_option("name", obj_name, "Element name")->required();
  wait_cmd->add_option("timeout", iarg1, "Timeout in miliseconds")->required();

  auto *image_cmd = appargs.add_subcommand("image", "Get element image");
  image_cmd->add_option("name", obj_name, "Element name")->required();
  image_cmd->add_option("filename", second_arg, "Filename to save the image to")
      ->required();

  auto *at_cmd = appargs.add_subcommand("at", "Get element at");
  at_cmd->add_option("x", iarg1, "Element x position")->required();
  at_cmd->add_option("y", iarg2, "Element y position")->required();

  CLI11_PARSE(appargs, argc, argv);

  QPuppetMasterClient client(QString::fromStdString(host), port, format);

  if (*liste_cmd) {
    auto resp = client.listElements(QString::fromStdString(second_arg),
                                    QString::fromStdString(obj_name));

    return showResponse(resp, format);
  } else if (*listw_cmd) {
    auto resp = client.listWindows(QString::fromStdString(second_arg));
    return showResponse(resp, format);
  } else if (*getp_cmd) {
    auto resp = client.getElementProperties(QString::fromStdString(obj_name),
                                            QString::fromStdString(second_arg));
    return showResponse(resp, format);
  } else if (*click_cmd) {
    auto resp = client.clickOnElement(QString::fromStdString(obj_name));
    return showResponse(resp, format);
  } else if (*press_cmd) {
    auto resp = client.pressElement(QString::fromStdString(obj_name));
    return showResponse(resp, format);
  } else if (*release_cmd) {
    auto resp = client.releaseElement(QString::fromStdString(obj_name));
    return showResponse(resp, format);
  } else if (*input_cmd) {
    auto resp = client.sendTextToElement(QString::fromStdString(obj_name),
                                         QString::fromStdString(second_arg));
    return showResponse(resp, format);
  } else if (*clear_cmd) {
    auto resp = client.clearElement(QString::fromStdString(obj_name));
    return showResponse(resp, format);
  } else if (*select_cmd) {
    auto resp =
        client.selectListItem(QString::fromStdString(obj_name), iarg1, iarg2);
    return showResponse(resp, format);
  } else if (*items_cmd) {
    auto resp = client.getElementItems(QString::fromStdString(obj_name));
    return showResponse(resp, format);
  } else if (*wait_cmd) {
    auto resp = client.waitForElement(QString::fromStdString(obj_name), iarg1);
    return showResponse(resp, format);
  } else if (*image_cmd) {
    auto resp = client.getElementImage(QString::fromStdString(obj_name));
    if (!resp.valid) {
      return -1;
    }

    QByteArray ba = resp.list[0].toByteArray();
    QFile file(QString::fromStdString(second_arg));
    if (!file.open(QFile::WriteOnly))
      return -1;

    file.write(ba);
    file.close();

    return 1;
  } else if (*at_cmd) {
    auto resp = client.getElementAt(iarg1, iarg2);
    return showResponse(resp, format);
  } else {
    return -1;
  }
}
